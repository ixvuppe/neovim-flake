{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
    neovim.url = "github:neovim/neovim?dir=contrib";
    treefmt-nix.url = "github:numtide/treefmt-nix";
  };
  outputs = {
    self,
    nixpkgs,
    utils,
    neovim,
    treefmt-nix,
    ...
  }:
    utils.lib.eachDefaultSystem (system: let
      inherit (nixpkgs) lib;
      pkgs = nixpkgs.legacyPackages.${system};
      treefmt-eval = treefmt-nix.lib.evalModule pkgs {
        projectRootFile = "flake.nix";
        programs = {
          alejandra.enable = true;
          stylua.enable = true;
        };
      };
      runtimeDeps = with pkgs; [
        # TreeSitter
        gcc

        # LSP
        lua-language-server
        nil
        clang-tools
        pyright
        ruff-lsp
      ];
      nvim =
        pkgs.wrapNeovimUnstable neovim.packages.${system}.neovim
        (pkgs.neovimUtils.makeNeovimConfig {
            customRC = ''
              set runtimepath^=${./.}
              source ${./.}/init.lua
            '';
          }
          // {
            wrapperArgs = [
              "--prefix"
              "PATH"
              ":"
              "${lib.makeBinPath runtimeDeps}"
            ];
          });
    in {
      overlays = {
        neovim = _: _prev: {
          neovim = nvim;
        };
        default = self.overlays.neovim;
      };

      packages = rec {
        neovim = nvim;
        default = neovim;
      };

      formatter = treefmt-eval.config.build.wrapper;

      devShells.default = pkgs.mkShell {
        packages = [
          treefmt-eval.config.build.wrapper
          self.packages.${system}.default
        ];
      };
    });
}
