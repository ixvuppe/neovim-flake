# ❄️ Ixvuppe's NeoVIM Flake

> Keep it simple, stupid.

![NeoVIM screenshot](./assets/neovim.webp)

My `NeoVIM` config built with [lazy.nvim](https://github.com/folke/lazy.nvim)
in a flake.

I tried to keep it as simple as possible, w/o using too many plugins.

# Features

These are the core plugins:

- `Catppuccin` colorscheme,
- `TreeSitter` for syntax highlighting,
- `LSP` and `autocompletion` via `lsp-zero`,
- `Telescope` fuzzy finder.

# Keymaps

Mostly I'm using default keymaps.
Also I'm using arrow keys intead of `hjkl` because I use 34-keys keyboard with
hands down rhodium layout.
I have arrow keys in the home-row in the navigation
layer.

|          Key |         Action |      Plugin |
|-------------:|---------------:|------------:|
|  `<leader>q` |           Quit |             |
|  `<leader>w` |          Write |             |
|          `u` |           Undo |             |
|          `r` |           Redo |             |
|     `<C-up>` |   Half page up |             |
|   `<C-down>` | Half page down |             |
|  `<leader>f` |     Find files | `Telescope` |
|  `<leader>b` |   Find buffers | `Telescope` |
| `<leader>la` |    Code action |       `LSP` |
| `<leader>ld` |    Diagnostics |       `LSP` |
| `<leader>lf` |    Format code |       `LSP` |

Everything else is default.

# Special thanks to:

- [IogaMaster's NeoVIM flake](https://github.com/IogaMaster/neovim)

# License

This repo licensed under the MIT license. See `LICENSE` file for more info.
