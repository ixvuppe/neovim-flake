return {
  {
    "VonHeikemen/lsp-zero.nvim",
    branch = "v3.x",
    lazy = true,
    config = false,
    init = function()
      vim.g.lsp_zero_extend_cmp = 0
      vim.g.lsp_zero_extend_lspconfig = 0
    end,
  },
  {
    "hrsh7th/nvim-cmp",
    event = "InsertEnter",
    dependencies = {
      "L3MON4D3/LuaSnip",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-buffer",
      "saadparwaiz1/cmp_luasnip",
    },
    config = function()
      local lsp_zero = require("lsp-zero")
      lsp_zero.extend_cmp()
      local ls = require("luasnip")
      local s = ls.snippet
      local i = ls.insert_node
      local f = ls.function_node
      local fmta = require("luasnip.extras.fmt").fmta
      ls.add_snippets("nix", {
        s(
          "module",
          fmta(
            [[
           {
             lib,
             config,
             pkgs,
             ...
           }:
           with lib; let
             cfg = config.<>;
           in {
             options.<> = {
               enable = mkEnableOption "<>";
             };

             config = mkIf cfg.enable {
               <>
             };
           }
         ]],
            {
              i(1),
              f(function(args, _)
                return args[1][1]
              end, { 1 }),
              i(2),
              i(0),
            }
          )
        ),
      })
      local cmp = require("cmp")
      local cmp_action = require("lsp-zero").cmp_action()
      cmp.setup({
        sources = {
          { name = "nvim_lsp" },
          { name = "luasnip" },
          { name = "path" },
          { name = "buffer", keyword_length = 3 },
        },
        window = {
          completion = cmp.config.window.bordered(),
          documentation = cmp.config.window.bordered(),
        },
        mapping = {
          ["<cr>"] = cmp.mapping.confirm({ select = false }),
          ["<c-up>"] = cmp.mapping.scroll_docs(-4),
          ["<c-down>"] = cmp.mapping.scroll_docs(4),
          ["<c-left>"] = cmp_action.luasnip_jump_backward(),
          ["<c-right>"] = cmp_action.luasnip_jump_forward(),
        },
      })
    end,
  },
  {
    "neovim/nvim-lspconfig",
    cmd = "LspInfo",
    event = { "BufReadPre", "BufNewFile" },
    dependencies = {
      { "hrsh7th/cmp-nvim-lsp" },
    },
    config = function()
      local lsp_zero = require("lsp-zero")
      lsp_zero.extend_lspconfig()

      lsp_zero.on_attach(function(_, bufnr)
        local opts = { buffer = bufnr }
        lsp_zero.default_keymaps(opts)
        vim.keymap.set("n", "<leader>la", vim.lsp.buf.code_action, opts)
        vim.keymap.set("n", "<leader>ld", vim.diagnostic.open_float, opts)
        vim.keymap.set("n", "<leader>lf", vim.lsp.buf.format, opts)
      end)

      vim.lsp.handlers["textDocument/hover"] =
        vim.lsp.with(vim.lsp.handlers.hover, { border = "rounded" })

      vim.lsp.handlers["textDocument/signatureHelp"] =
        vim.lsp.with(vim.lsp.handlers.signature_help, { border = "rounded" })

      require("lspconfig").lua_ls.setup(lsp_zero.nvim_lua_ls())
      require("lspconfig").nil_ls.setup({})
      require("lspconfig").clangd.setup({})
      require("lspconfig").pyright.setup({})
      require("lspconfig").ruff_lsp.setup({})
    end,
  },
}
