return {
  "stevearc/oil.nvim",
  dependencies = { "nvim-tree/nvim-web-devicons" },
  lazy = false, -- Won't work as default file explorer otherwise
  config = function()
    require("oil").setup({})
  end,
}
