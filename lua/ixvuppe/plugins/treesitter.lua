return {
  "nvim-treesitter/nvim-treesitter",
  event = { "BufReadPre", "BufNewFile" },
  dependencies = {
    "nvim-treesitter/nvim-treesitter-textobjects",
    "nushell/tree-sitter-nu",
    "IndianBoy42/tree-sitter-just",
  },
  build = ":TSUpdate",
  config = function()
    require("tree-sitter-just").setup({})
    require("nvim-treesitter.configs").setup({
      ensure_installed = {
        "lua",
        "python",
        "markdown",
        "markdown_inline",
        "nix",
        "bash",
        "nu",
        "c",
        "just",
      },
      highlight = {
        enable = true,
      },
      indent = {
        enable = true,
      },
      incremental_selection = {
        enable = true,
        keymaps = {
          init_selection = "<C-space>",
          node_incremental = "<C-space>",
          scope_incremental = false,
          node_decremental = "<bs>",
        },
      },
    })
  end,
}
