local function is_git_repo()
  vim.fn.system("git rev-parse --is-inside-work-tree")
  return vim.v.shell_error == 0
end

local function project_files()
  if is_git_repo() then
    require("telescope.builtin").git_files()
  else
    require("telescope.builtin").find_files()
  end
end

local function previous_buffers()
  require("telescope.builtin").buffers({
    sort_mru = true,
    ignore_current_buffer = true,
  })
end

return {
  "nvim-telescope/telescope.nvim",
  branch = "0.1.x",
  dependencies = { "nvim-lua/plenary.nvim" },
  keys = {
    { "<leader>f", project_files, desc = "telescope project files" },
    { "<leader>b", previous_buffers, desc = "telescope previous buffers" },
  },
}
