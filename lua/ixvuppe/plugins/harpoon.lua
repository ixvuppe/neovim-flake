return {
  "theprimeagen/harpoon",
  branch = "harpoon2",
  dependencies = { "nvim-lua/plenary.nvim" },
  config = function()
    require("harpoon"):setup()
  end,
  keys = {
    {
      "<leader>a",
      desc = "Harpoon file",
      function()
        require("harpoon"):list():append()
      end,
    },
    {
      "<leader>A",
      desc = "Harpoon file",
      function()
        local harpoon = require("harpoon")
        harpoon.ui:toggle_quick_menu(harpoon:list())
      end,
    },
    {
      "<leader>d",
      desc = "Harpoon 1st file",
      function()
        require("harpoon"):list():select(1)
      end,
    },
    {
      "<leader>t",
      desc = "Harpoon 2st file",
      function()
        require("harpoon"):list():select(2)
      end,
    },
    {
      "<leader>n",
      desc = "Harpoon 3st file",
      function()
        require("harpoon"):list():select(3)
      end,
    },
    {
      "<leader>s",
      desc = "Harpoon 4st file",
      function()
        require("harpoon"):list():select(4)
      end,
    },
  },
}
