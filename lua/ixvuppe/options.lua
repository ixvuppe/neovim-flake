vim.g.mapleader = " "

vim.opt.clipboard = "unnamedplus"

vim.opt.termguicolors = true

vim.opt.undofile = true

vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.fillchars = { eob = " " }

vim.opt.cmdheight = 0

vim.opt.scrolloff = 8

vim.opt.smoothscroll = true

vim.opt.colorcolumn = "80"

vim.opt.list = true
vim.opt.listchars = {
  tab = "⇥ ",
  trail = "␣",
  nbsp = "⍽",
}

vim.opt.expandtab = true
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2

vim.opt.hlsearch = false
vim.opt.smartcase = true
vim.opt.ignorecase = true
vim.opt.incsearch = true

vim.opt.splitright = true
vim.opt.splitbelow = true
