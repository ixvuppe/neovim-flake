vim.keymap.set("n", "<leader>q", "<cmd>q<cr>", { desc = "quit" })
vim.keymap.set("n", "<leader>w", "<cmd>w<cr>", { desc = "write" })

vim.keymap.set("n", "u", "<cmd>undo<cr>", { desc = "undo" })
vim.keymap.set("n", "U", "<cmd>redo<cr>", { desc = "redo" })

vim.keymap.set("n", "<c-up>", "<c-u>", { desc = "half-page up" })
vim.keymap.set("n", "<c-down>", "<c-d>", { desc = "halp-page down" })
